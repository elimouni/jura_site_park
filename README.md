# Jura Site Park

## Installation

To install the website, it is easy. First make sure you already installed composer.
Then just clone the repository :

```bash
git clone https://gitlab.com/elimouni/jura_site_park
```

Then go inside the directory, and use composer to install all the dependencies :

```bash
composer update
```

And launch the PHP server :

```bash
php -S 127.0.0.1:8000
```

## Deployment

Deploying the website will require some changes in the views on links and redirections.
