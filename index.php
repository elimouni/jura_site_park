<?php
use Michelf\Markdown;
require "vendor/autoload.php";

// Loading and configuration of Twig
$loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

// Adding it as an extension in Flight
Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension()); // Add the debug extension
});

// Adding the path to the main page view
Flight::route('GET /', function(){
    //Get all the dinausor's presentation
    $data = array('data' => json_decode(Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')->body));
    Flight::view()->display('index.twig', $data);
});

// Adding the path to the dinosaur's view page
Flight::route('GET /dinosaur/@name', function($name){
    //Get the dinosaur's profile
    $data = json_decode(Requests::get("https://allosaurus.delahayeyourself.info/api/dinosaurs/$name")->body);
    // Verify if the dinosaur exists
    if($data == NULL){
        Flight::redirect('/');
        echo 'oui';
    }
    else{
        //Transform the Markdown in HTML
        $data->description = Markdown::defaultTransform($data->description);
        //Generate the propositions
        $propositions = get_three_profiles($name);
        //Remake the array
        $data = array('data' => $data, 'propositions' => $propositions);
        Flight::view()->display('dinosaur.twig', $data);
    }
});

//Start Flight module
Flight::start();