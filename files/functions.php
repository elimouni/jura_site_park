<?php

function get_three_profiles($name_to_escape){
    //We get all the profiles of dinosaurs
    $data = json_decode(Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')->body);
    //We escape the dinosaur of the actual page
    foreach ($data as $key => $dino){
        if($dino->slug == $name_to_escape) {
            unset($data[$key]);
        }
    }
    //We generate 3 random keys
    $random_keys = array_rand($data, 3); // On récupére trois clés aléatoires
    //We rewrite data using the new keys
    $data = array('0' => $data[$random_keys[0]], '1' => $data[$random_keys[1]], '2' => $data[$random_keys[2]]);
    //Reformat to allow to use in Twig
    return $data;
}